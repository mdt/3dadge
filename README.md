# Badge aus dem 3D Drucker

## Ideen Expo

Während der Ideen Expo in Hannover haben wir mit Kindern Bausätze
zusammengelötet.

<img src='media/20220718_164009.jpg' width='50%' />

Das kam so gut an, dass alle 80 am ersten Tag weg waren. Am zweiten Tag haben
wir dann Draht-Skulpturen gelötet. Löten verbindet! So schön die Bausätze
gearbeitet waren und das Löten Spass machte und die Teilnehmerinnen oder ihre
Eltern reagierten, dass der Katze die Sonne aus dem Po scheint, fand ich die
konfektionierten Sets im Nachhinein unpassend. Ich dachte, da geht noch ein
wenig mehr Kreativität, Nachhaltigkeit und Inspiration.

## Ein Badge aus dem 3-D-Drucker

Ich habe überlegt, ob man so etwas wie die Platine, die die Bauteile hält nicht
mit dem 3-D Drucker erstellen kann, Löcher vorsieht, in die die LEDs gesteckt
werden, sodaß sie durch's Material leuchten und alles mit Draht verlötet, den
man so durch Löcher am Batteriefach steckt, dass die Spannung abgegriffen wird.
Am liebsten aus Teilen vom E-Schrott, das ist nachhaltig und übt das Auslöten.

<img src='media/20220808_132319.jpg' width='50%' />

Schnell ist ein Model erzeugt, inzwischen weiss ich wie ich aus dem stl den
nötigen gcode erzeugen kann. Ich habe neuerdings einen 3D Drucker, ein altes
Modell, das sein Besitzer nicht mehr haben wollte, weil er einen neuen gekauft
hat. Hier erlaubt er mir Drucken zu lernen.

## Illuminiert mit LEDs

Das erste Model war viel zu groß, daran muss ich noch üben. Zu erkennen, wie
groß das finale Stück sein soll. Das zweite Model war schon besser.

<img src='media/20220808_121026.jpg' width='50%' />

Aus dem Ausdruck muss die Stützstruktur entfernt werden - es gib Aussparungen
für die LEDs hinter den Augen, eine Aussparung für eine CR2032 und Löcher
für Draht um die Spannung an der Batterie abzugreifen.

<img src='media/20220808_121055.jpg' width='50%' />

Es bietet sich an, alte Bauteile aus Elektro-Schrott auszulöten.

<img src='media/20220808_121149.jpg' width='50%' />

Die Bauteile passen in die vorbereiten Löcher, ich habe zum Entfernen der
Stützstruktur in den Löchern einen 5mm Borer zuhilfe genommen.

<img src='media/20220808_132249.jpg' width='50%' />

Der Draht wird durch die Löcher gezogen, dass die Plus- und Minus-Pole der
Batterie verbunden sind.

<img src='media/20220808_134549.jpg' width='50%' />

Man kann den Draht um die Füße der Bauteile wickeln um das Löten zu
erleichtern.

<img src='media/20220808_180350.jpg' width='50%' />

Die Batterie habe ich aus dem Sondermüll - für die zwei LEDs hat sie noch genug
Restladung. Sie wird einfach in das vorgesehene Loch gedrückt, hält
ausreichend fest und findet Kontakt.

Grundsätzlich finde ich es fragwürdig Dinge zu bauen, die eine solche
Sondermülldeponie wir eine CR-2032 brauchen. Ein ökologische Variante wäre mir
lieber.

<img src='media/20220808_180535.jpg' width='50%' />

Vorn kann man mit Filzstiften Farbe aufbringen, die Qualität meiner Handykamera
ist allerdings begrenzt, das gut abzubilden.

<img src='media/20220808_190431.jpg' width='50%' />

## Schaltplan

<img src='media/schaltplan.jpg' width='50%' />

Wichtig ist, dass jede LED einen eigenen Widerstand bekommt (auch LEDs sind
Dioden, die in eine Richtung sperren und in die andere durchlassen. Dummer
Weise leuchten sie in der Richtung, in der sie durchlassen, sodass dies einem
Kurzschluss gleichkommt. Ohne Widerstand, der den Strom begrenzt sind sie
schnell kaputt). Ich habe Widerstände um die 50-100 Ohm genommen (da die eine
LED heller leuchtete hat die den größeren Widerstand bekommen). Die LEDs haben
Plus- und Minus-Pole, die nicht vertauscht werden sollten. Ich habe einfach
getestet in welcher Polung sie leuchten (neue LEDs haben ein längeres Beinchen
für den Plus-Pol).

Den Widerständen ist die Richtung egal, der Batterie allerdings auch
nicht, da ist die große Kappe Plus und der kleinere innenliegende Teil Minus
(steht aber auch drauf).

Die Drähte werden so eingesteckt, dass der eine Draht den Plus-Pol an der Seite
berührt (ich habe den Draht ein wenig aufgezwirbelt, da er zu dünn war), der
Minus-Pol nur den Boden berührt. Letzterer kann in ein Loch weiter zur Mitte
gesteckt werden, damit er nicht zum Rand wandert und einen Kurzschluss auslöst.

## Making

Ich nutze [OpenScad](https://openscad.org/) um das [Model](3dadge.stl) zu
erstellen, der [Code](3dadge.scad) ist einfach zu lesen und ruft nach
Erweiterungen.

<img src='media/3dadge.png' width='50%' />

Ein Schalter wäre nicht schlecht.. vielleicht einfach aus dem Draht biegen zum
einhaken?

# Lizenz

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/legalcode.de"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz</a>.

