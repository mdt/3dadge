$fn=40;
let(ah=90, px=11, ih=40){
	difference(){
		union() {
			let(s=1.4, r=sqrt(ih*ih*2)/2){
				intersection(){
					translate([-ih/2*s, -ih/2*s, 7])
						scale([ih/ah*s, ih/ah*s, 0.09])
							surface("alien.png");
					cylinder(20, r, r);
				}
				cylinder(8, r, r); // badge base
			}
		}
		union(){
			translate([0, ih/2+5, -1]) cylinder(20, 2, 2); // hook
			translate([ih/px*3, -ih/px, -1]) cylinder(12, 2.5, 2); // led
			translate([-ih/px*3, -ih/px, -1]) cylinder(12, 2.5, 2); // led
			translate([0, 7, -1]) cylinder(4, 10, 10); // battery
			translate([0, 17, -1]) cylinder(30, 1, 1); // bat + in
			translate([0, 20, -1]) cylinder(30, 1, 1); // bat + out
			translate([10, 0, -1]) cylinder(30, 1, 1); // bat - in
			translate([0, 0, -1]) cylinder(30, 1, 1); // bat - out
			translate([0, 2, -1]) cylinder(7, 1, 1); // bat - out
		}
	}
}
